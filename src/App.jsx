import React, { useEffect, useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { Snackbars } from './common/Alert/Alert';
import { useRoutes } from './routes';
import Header from './components/Header/Header';
import { fetchedUserProfile } from './services';
import { getUser } from './store/user/selectors';
import { setUserProfile, userLogIn } from './store/user/actionCreators';
import { storageName } from './constants';
import { getMessage } from './store/messages/selectors';

import './App.css';

function App() {
	const dispatch = useDispatch();
	const userData = useSelector(getUser);
	const isAuthenticated = userData.isAuth;
	const routes = useRoutes(isAuthenticated);
	const [ready, setReady] = useState(false);
	const message = useSelector(getMessage);

	useEffect(() => {
		const data = JSON.parse(localStorage.getItem(storageName));

		if (data && data.token) {
			dispatch(userLogIn({ token: data.token, ...data.user }));
			fetchedUserProfile(data.token)
				.then((userProfile) => {
					dispatch(
						setUserProfile({
							name: userProfile.result.name,
							email: userProfile.result.email,
							role: userProfile.result.role,
						})
					);
				})
				.catch((e) => {
					throw e;
				});
		}
		setReady(true);
	}, []);

	return (
		<div className='container'>
			{isAuthenticated && <Header />}
			<BrowserRouter>{ready && routes}</BrowserRouter>
			{message.show && (
				<Snackbars
					severity={message.severity}
					message={message.text}
					show={true}
				/>
			)}
		</div>
	);
}

export default App;
