import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';

import CourseCard from '../CourseCard';
import Courses from '../../../Courses';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Oleksii Shevchenko',
	},
	courses: [
		{
			id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
			title: 'JavaScript Advanced',
			description: `JavaScript Course Description`,
			creationDate: '8/3/2021',
			duration: 111,
			authors: [
				'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
				'9987de6a-b475-484a-b885-622b8fb88bda',
			],
		},
	],
	authors: [
		{
			name: 'author',
			id: '9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
		},
		{
			name: 'author2',
			id: '1c972c52-3198-4098-b6f7-799b45903199',
		},
		{
			name: 'author3',
			id: '072fe3fc-e751-4745-9af5-aa9eed0ea9ed',
		},
		{
			name: 'author4',
			id: '40b21bd5-cbae-4f33-b154-0252b1ae03a9',
		},
		{
			name: 'author5',
			id: '5e0b0f18-32c9-4933-b142-50459b47f09e',
		},
		{
			name: 'author6',
			id: '9987de6a-b475-484a-b885-622b8fb88bda',
		},
	],
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

describe('CourseCard', () => {
	it('should display title', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses />
				</Provider>
			</BrowserRouter>
		);
		const title = screen.getByText('JavaScript Advanced');
		expect(title).toBeInTheDocument();
	});

	it('should display description', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses />
				</Provider>
			</BrowserRouter>
		);
		const description = screen.getByText('JavaScript Course Description');
		expect(description).toBeInTheDocument();
	});

	it('should display duration in the correct format', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses />
				</Provider>
			</BrowserRouter>
		);
		const duration = screen.getByText('01:51 hours');
		expect(duration).toBeInTheDocument();
	});

	it('should display authors list', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses />
				</Provider>
			</BrowserRouter>
		);
		const authorsList = screen.getByText('author, author6');
		expect(authorsList).toBeInTheDocument();
	});

	it('should display created date in the correct format.', () => {
		render(
			<BrowserRouter>
				<Provider store={mockedStore}>
					<Courses />
				</Provider>
			</BrowserRouter>
		);
		const createdDate = screen.getByText('8.3.2021');
		expect(createdDate).toBeInTheDocument();
	});
});
