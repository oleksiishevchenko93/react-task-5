import React from 'react';
import PropTypes from 'prop-types';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';
import { BUTTONS_TEXT, LABEL_TEXT } from '../../../../constants';

import styles from './SearchBar.module.css';

const SearchBar = ({ onChange, onClick, value }) => {
	const { SEARCH } = BUTTONS_TEXT;
	const { SEARCH_VALUE } = LABEL_TEXT;
	return (
		<div className={styles.container}>
			<Input
				placeholderText={SEARCH_VALUE}
				onChange={onChange}
				value={value}
				name={SEARCH}
			/>
			<Button callback={onClick} buttonText={SEARCH} />
		</div>
	);
};

SearchBar.propTypes = {
	onChange: PropTypes.func.isRequired,
	onClick: PropTypes.func.isRequired,
	value: PropTypes.string.isRequired,
};

export default SearchBar;
