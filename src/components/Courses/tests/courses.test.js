import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { Route, Routes } from 'react-router';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { createBrowserHistory } from 'history';
import userEvent from '@testing-library/user-event';

import Courses from '../Courses';
import CourseForm from '../../CreateCourse/CourseForm';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Oleksii Shevchenko',
	},
	courses: [
		{
			id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
			title: 'JavaScript Advanced',
			description: `JavaScript Course Description`,
			creationDate: '8/3/2021',
			duration: 111,
			authors: [
				'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
				'9987de6a-b475-484a-b885-622b8fb88bda',
			],
		},
		{
			id: 'de5aaa59-90f5-4dbc-b8a7-aaf205c551bn',
			title: 'CSS Advanced',
			description: `JavaScript Course Description`,
			creationDate: '8/9/2021',
			duration: 182,
			authors: [
				'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
				'9987de6a-b475-484a-b885-622b8fb88bda',
			],
		},
		{
			id: 'de5aaa59-90f5-4dbc-b8a8-aaf205c551bu',
			title: 'React Advanced',
			description: `JavaScript Course Description`,
			creationDate: '4/7/2021',
			duration: 214,
			authors: [
				'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
				'9987de6a-b475-484a-b885-622b8fb88bda',
			],
		},
	],
	authors: [
		{
			name: 'author',
			id: '9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
		},
		{
			name: 'author2',
			id: '1c972c52-3198-4098-b6f7-799b45903199',
		},
		{
			name: 'author3',
			id: '072fe3fc-e751-4745-9af5-aa9eed0ea9ed',
		},
		{
			name: 'author4',
			id: '40b21bd5-cbae-4f33-b154-0252b1ae03a9',
		},
		{
			name: 'author5',
			id: '5e0b0f18-32c9-4933-b142-50459b47f09e',
		},
		{
			name: 'author6',
			id: '9987de6a-b475-484a-b885-622b8fb88bda',
		},
	],
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

test('should be in the document', async () => {
	render(
		<BrowserRouter>
			<Provider store={mockedStore}>
				<Courses />
			</Provider>
		</BrowserRouter>
	);
	const courses = screen.getByTestId('courses');
	expect(courses).toBeInTheDocument();
});

test('should display amount of CourseCard equal length of courses array', () => {
	render(
		<BrowserRouter>
			<Provider store={mockedStore}>
				<Courses />
			</Provider>
		</BrowserRouter>
	);
	const amountOfCourseCard = screen.getAllByTestId('courseCard');
	expect(amountOfCourseCard.length).toEqual(
		mockedStore.getState().courses.length
	);
});

test('should display Empty container if courses array length is 0', () => {
	const mockedState = {
		user: {
			isAuth: true,
			name: 'Oleksii Shevchenko',
		},
		courses: [],
		authors: [],
	};

	const mockedStore = {
		getState: () => mockedState,
		subscribe: jest.fn(),
		dispatch: jest.fn(),
	};

	render(
		<BrowserRouter>
			<Provider store={mockedStore}>
				<Courses />
			</Provider>
		</BrowserRouter>
	);
	const coursesList = screen.getByTestId('coursesList');
	expect(coursesList).toBeEmptyDOMElement();
});

test('CourseForm should be showed after a click on a button "Add new course"', async () => {
	const history = createBrowserHistory();
	history.push('/courses');
	render(
		<BrowserRouter location={history.location} navigator={history}>
			<Provider store={mockedStore}>
				<Routes>
					<Route path='/courses' element={<Courses />} />
					<Route path='/courses/add' element={<CourseForm />} />
				</Routes>
			</Provider>
		</BrowserRouter>
	);
	const addCourseButton = screen.getByText(/Add course/i);
	userEvent.click(addCourseButton);
	const courseForm = screen.getByTestId('courseForm');
	expect(courseForm).toBeInTheDocument();
});
