import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import UserItem from './components/UserItem/UserItem';
import { getDuration } from '../../helpers/pipeDuration';
import { BUTTONS_TEXT, LABEL_TEXT } from '../../constants';
import { validateCourse } from '../../helpers/courseValidator';
import { getAuthors } from '../../store/authors/selectors';
import { generateDate } from '../../helpers/dateGenerator';
import { getUser } from '../../store/user/selectors';
import { getCourses } from '../../store/courses/selectors';
import { addCourseThunk, updateCourseThunk } from '../../store/courses/thunk';
import { addAuthorThunk } from '../../store/authors/thunk';
import {
	changeMessageSeverity,
	changeMessageText,
	hideMessage,
	showMessage,
} from '../../store/messages/actionCreators';
import { asyncHideMessage } from '../../store/messages/thunk';

import styles from './CourseForm.module.css';

const CourseForm = () => {
	const authorsList = useSelector(getAuthors);
	const [newAuthor, setNewAuthor] = useState({
		id: '',
		name: '',
	});
	const { CREATE_COURSE, CREATE_AUTHOR, ADD_AUTHOR, DELETE_AUTHOR } =
		BUTTONS_TEXT;
	const { TITLE, AUTHOR_NAME, DESCRIPTION, DURATION } = LABEL_TEXT;
	const [course, setCourse] = useState({
		id: '',
		title: 'title',
		description: 'description',
		creationDate: '',
		duration: 65,
		authors: [],
	});
	const { title, description, duration, authors } = course;
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const userData = useSelector(getUser);
	const { courseId } = useParams();
	const [update, setUpdate] = useState(false);
	const courses = useSelector(getCourses);

	useEffect(() => {
		if (courseId) {
			const courseById = courses.find((course) => course.id === courseId);
			setCourse(courseById);
			setUpdate(true);
		} else {
			setUpdate(false);
		}
	}, [courseId, courses]);

	const addCourse = () => {
		const isValid = validateCourse(course);
		dispatch(hideMessage());
		if (!isValid.success) {
			dispatch(changeMessageText(isValid.message));
			dispatch(changeMessageSeverity('error'));
			dispatch(showMessage());
			return;
		}
		const newCourse = {
			...course,
			duration: +course.duration,
			id: uuidv4(),
			creationDate: generateDate(),
		};
		dispatch(addCourseThunk(newCourse, userData.token));
		navigate('/courses');
		setCourse({
			id: '',
			title: '',
			description: '',
			creationDate: '',
			duration: 0,
			authors: [],
		});
	};

	const updateCurrentCourse = () => {
		const isValid = validateCourse(course);
		dispatch(hideMessage());
		if (!isValid.success) {
			dispatch(changeMessageText(isValid.message));
			dispatch(changeMessageSeverity('error'));
			dispatch(showMessage());
			return;
		}

		const updatedCourse = { ...course, duration: +course.duration };
		dispatch(updateCourseThunk(updatedCourse, userData.token));

		navigate('/courses');
		setCourse({
			id: '',
			title: '',
			description: '',
			creationDate: '',
			duration: 0,
			authors: [],
		});
	};

	const addAuthor = (author) => {
		if (course.authors.includes(author.id)) {
			dispatch(changeMessageText('This author has been already added!'));
			dispatch(changeMessageSeverity('warning'));
			dispatch(showMessage());
			dispatch(asyncHideMessage());
			return;
		}
		setCourse({
			...course,
			authors: [...course.authors, author.id],
		});
		dispatch(changeMessageText(`${author.name} has been added successfully!`));
		dispatch(changeMessageSeverity('success'));
		dispatch(showMessage());
		dispatch(asyncHideMessage());
	};

	const deleteAuthor = (author) => {
		const result = course.authors.filter((authorId) => {
			return authorId !== author.id;
		});

		setCourse({
			...course,
			authors: result,
		});
		dispatch(
			changeMessageText(`${author.name} has been removed successfully!`)
		);
		dispatch(changeMessageSeverity('success'));
		dispatch(showMessage());
		dispatch(asyncHideMessage());
	};

	const handleChangeCourse = (event) => {
		setCourse({
			...course,
			[event.target.name]: event.target.value,
		});
	};

	const createAuthor = async () => {
		if (newAuthor.name.trim()?.length <= 2) {
			dispatch(changeMessageText('Invalid Data!'));
			dispatch(changeMessageSeverity('warning'));
			dispatch(showMessage());
			dispatch(asyncHideMessage());
			return;
		}
		dispatch(
			addAuthorThunk(
				{
					...newAuthor,
					id: uuidv4(),
				},
				userData.token
			)
		);
		dispatch(
			changeMessageText(`${newAuthor.name} has been created successfully!`)
		);
		dispatch(changeMessageSeverity('success'));
		dispatch(showMessage());
		dispatch(asyncHideMessage());
		setNewAuthor({
			id: '',
			name: '',
		});
	};

	const allAuthors = authorsList.map((author, index) => {
		return (
			<UserItem
				key={author.id + index}
				buttonText={ADD_AUTHOR}
				cb={addAuthor}
				author={author}
				className={'green'}
			/>
		);
	});

	const coursesAuthors = authors.map((authorId) => {
		const author = authorsList.find((element) => {
			return element.id === authorId;
		});
		return (
			<UserItem
				key={author.id}
				buttonText={DELETE_AUTHOR}
				cb={deleteAuthor}
				author={author}
				className={'red'}
			/>
		);
	});

	return (
		<div className={styles.container} data-testid='courseForm'>
			<div className={styles.description__content}>
				<Input
					name='title'
					value={title}
					onChange={handleChangeCourse}
					labelText={TITLE}
					placeholderText={TITLE}
				/>
				{!update ? (
					<Button buttonText={CREATE_COURSE} callback={addCourse} />
				) : (
					<Button
						buttonText={'update course'}
						callback={updateCurrentCourse}
						className={'green'}
					/>
				)}
			</div>
			<div className={styles.description__content}>
				<label className={styles.label} htmlFor='textarea'>
					{DESCRIPTION}
				</label>
				<textarea
					className={styles.textarea}
					name='description'
					id='textarea'
					rows='5'
					value={description}
					placeholder={DESCRIPTION}
					onChange={handleChangeCourse}
				/>
			</div>
			<div className={styles.authors__content}>
				<div className={styles.left__box}>
					<div className={styles.add__author}>
						<h2>Add Author</h2>
						<Input
							value={newAuthor.name}
							placeholderText={AUTHOR_NAME}
							labelText={AUTHOR_NAME}
							name={'newAuthor'}
							onChange={(event) =>
								setNewAuthor({ ...newAuthor, name: event.target.value })
							}
						/>
						<Button
							callback={createAuthor}
							buttonText={CREATE_AUTHOR}
							className={'blue'}
						/>
					</div>
					<div className={styles.duration}>
						<h2>{DURATION}</h2>
						<Input
							name='duration'
							value={duration}
							placeholderText={DURATION}
							labelText={DURATION}
							onChange={handleChangeCourse}
							type='number'
						/>
						<p className={styles.duration__value}>
							{DURATION}: <strong>{getDuration(duration)}</strong> hours
						</p>
					</div>
				</div>
				<div className={styles.right__box}>
					<div className={styles.authors}>
						<h2>Authors</h2>
						{allAuthors}
					</div>
					<div className={styles.authors__list}>
						<h2>Course authors</h2>
						{!authors.length && <p>Authors list is empty</p>}
						{!!authors.length && coursesAuthors}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CourseForm;
