import React from 'react';
import { Provider } from 'react-redux';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import CourseForm from '../CourseForm';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Oleksii Shevchenko',
	},
	courses: [],
	authors: [],
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

describe('CourseForm', () => {
	it('should be in the document', () => {
		render(
			<Provider store={mockedStore}>
				<CourseForm />
			</Provider>
		);
		const courseForm = screen.getByTestId('courseForm');
		expect(courseForm).toBeInTheDocument();
	});
});
