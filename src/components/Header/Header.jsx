import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { userLogOut } from '../../store/user/actionCreators';
import { getUser } from '../../store/user/selectors';
import { logoutUserThunk } from '../../store/user/thunk';

import styles from './Header.module.css';

const Header = () => {
	const dispatch = useDispatch();
	const userData = useSelector(getUser);

	const logout = () => {
		dispatch(logoutUserThunk(userData.token));
		dispatch(userLogOut());
		localStorage.clear();
	};

	return (
		<div className={styles.header} data-testid='header'>
			<Logo />
			<div className={styles.userName}>
				<h2>{userData.name || 'Admin'}</h2>
				<Button buttonText='log out' className={'green'} callback={logout} />
			</div>
		</div>
	);
};

export default Header;
