import React from 'react';
import { Provider } from 'react-redux';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import Header from '../Header';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Oleksii Shevchenko',
	},
	courses: [],
	authors: [],
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

describe('Header', () => {
	it('should be in the document', () => {
		render(
			<Provider store={mockedStore}>
				<Header />
			</Provider>
		);
		const header = screen.getByTestId('header');
		expect(header).toBeInTheDocument();
	});

	it('should have a logo', () => {
		render(
			<Provider store={mockedStore}>
				<Header />
			</Provider>
		);
		const logo = screen.getByAltText('courses_logo');
		expect(logo).toBeInTheDocument();
	});

	it('should have user name', () => {
		render(
			<Provider store={mockedStore}>
				<Header />
			</Provider>
		);
		const userName = screen.getByText('Oleksii Shevchenko');
		expect(userName).toBeInTheDocument();
	});
});
