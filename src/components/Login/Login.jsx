import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import Form from '../../common/Form/Form';
import { BUTTONS_TEXT } from '../../constants';
import { loginUserThunk } from '../../store/user/thunk';
import {
	changeMessageSeverity,
	changeMessageText,
	showMessage,
} from '../../store/messages/actionCreators';
import { asyncHideMessage } from '../../store/messages/thunk';

const Login = () => {
	const [form, setForm] = useState({
		email: '',
		password: '',
	});
	const { LOGIN_LINK, LOGIN_BUTTON } = BUTTONS_TEXT;
	const dispatch = useDispatch();

	const loginHandler = async (event) => {
		event.preventDefault();
		if (!form.email || !form.password) {
			const field = !form.email ? 'email' : 'password';
			const message = `${field} field is empty`;
			dispatch(changeMessageText(message));
			dispatch(changeMessageSeverity('error'));
			dispatch(showMessage());
			dispatch(asyncHideMessage());
			return;
		}
		dispatch(loginUserThunk(form));
	};

	const handleChange = (event) => {
		setForm({
			...form,
			[event.target.name]: event.target.value,
		});
	};

	return (
		<div className={'login'}>
			<Form
				linkText={LOGIN_LINK}
				onChange={handleChange}
				buttonText={LOGIN_BUTTON}
				onSubmit={loginHandler}
				linkPath={'/registration'}
				form={form}
			/>
		</div>
	);
};

export default Login;
