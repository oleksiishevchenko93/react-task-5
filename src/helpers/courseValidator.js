export const validateCourse = (course) => {
	const result = {
		message: '',
		success: false,
	};

	if (course.title.trim()?.length <= 2) {
		result.message = '"Title" field is invalid!';
		return result;
	}
	if (course.description.trim()?.length <= 2) {
		result.message = '"Description" field is invalid!';
		return result;
	}
	if (course.duration <= 0) {
		result.message = '"Duration" field should be more than 0!';
		return result;
	}
	if (!course.authors.length) {
		result.message = '"Authors" field is invalid!';
		return result;
	}
	result.success = true;
	return result;
};
