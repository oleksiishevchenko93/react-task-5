import React from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import Courses from './components/Courses/Courses';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';
import CourseForm from './components/CreateCourse/CourseForm';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { getUser } from './store/user/selectors';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';

export const useRoutes = () => {
	const userData = useSelector(getUser);
	if (userData.isAuth) {
		return (
			<Routes>
				<Route path='/' element={<Navigate to='courses' />} />
				<Route path='courses' element={<Courses />} />
				<Route path='courses/:courseId' element={<CourseInfo />} />
				<Route
					path='courses/add'
					element={
						<PrivateRoute>
							<CourseForm />
						</PrivateRoute>
					}
				/>
				<Route
					path='courses/update/:courseId'
					element={
						<PrivateRoute>
							<CourseForm />
						</PrivateRoute>
					}
				/>
				<Route path='*' element={<Navigate to='courses' />} />
			</Routes>
		);
	}

	return (
		<Routes>
			<Route path='login' element={<Login />} />
			<Route path='registration' element={<Registration />} />
			<Route path='*' element={<Navigate to='login' />} />
		</Routes>
	);
};
