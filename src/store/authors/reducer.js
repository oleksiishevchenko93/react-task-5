import { ADD_AUTHOR, GET_AUTHORS } from './actionTypes';

const authorsInitialState = [
	{
		name: '',
		id: '',
	},
];

export const authorsReducer = (state = authorsInitialState, action) => {
	switch (action.type) {
		case ADD_AUTHOR:
			console.log(action.payload);
			return [...state, action.payload];
		case GET_AUTHORS:
			return action.payload;
		default:
			return state;
	}
};
