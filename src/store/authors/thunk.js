import { fetchedAddAuthor, fetchedAuthors } from '../../services';
import { addAuthorToCourse, setAuthors } from './actionCreators';
import {
	changeMessageSeverity,
	changeMessageText,
	showMessage,
} from '../messages/actionCreators';
import { asyncHideMessage } from '../messages/thunk';

export const getAuthorsThunk = (token) => {
	return (dispatch) => {
		fetchedAuthors(token)
			.then((authors) => {
				dispatch(setAuthors(authors));
			})
			.catch((e) => {
				dispatch(changeMessageText(e.message));
				dispatch(changeMessageSeverity('error'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			});
	};
};
export const addAuthorThunk = (author, token) => {
	return (dispatch) => {
		fetchedAddAuthor(author, token)
			.then(() => {
				dispatch(addAuthorToCourse(author));
				dispatch(
					changeMessageText(`You have added ${author.name} successfully!`)
				);
				dispatch(changeMessageSeverity('success'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			})
			.catch((e) => {
				dispatch(changeMessageText(e.message));
				dispatch(changeMessageSeverity('error'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			});
	};
};
