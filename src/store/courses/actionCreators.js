import {
	ADD_COURSE,
	DELETE_COURSE,
	GET_COURSES,
	UPDATE_COURSE,
} from './actionTypes';

export const setCourses = (courses) => {
	return {
		type: GET_COURSES,
		payload: courses,
	};
};

export const addNewCourse = (course) => {
	return {
		type: ADD_COURSE,
		payload: course,
	};
};

export const deleteCourse = (courseId) => {
	return {
		type: DELETE_COURSE,
		payload: courseId,
	};
};

export const updateCourse = (updatedCourse) => {
	return {
		type: UPDATE_COURSE,
		payload: updatedCourse,
	};
};
