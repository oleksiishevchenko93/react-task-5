import React from 'react';
import '@testing-library/jest-dom';
import { coursesReducer } from '../courses/reducer';

describe('reducer', () => {
	it('should return the initial state', () => {
		expect(coursesReducer(undefined, {})).toEqual([]);
	});

	it('should handle SAVE_COURSE and returns new state', () => {
		expect(
			coursesReducer(
				[
					{
						id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c55134',
						title: 'JavaScript Advanced',
						description: `JavaScript Course Description`,
						creationDate: '10/6/2022',
						duration: 120,
						authors: [
							'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
							'9987de6a-b475-484a-b885-622b8fb88bda',
						],
					},
				],
				{
					type: 'ADD_COURSE',
					payload: {
						id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c55135',
						title: 'React Advanced',
						description: `React Course Description`,
						creationDate: '11/6/2022',
						duration: 120,
						authors: [
							'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
							'9987de6a-b475-484a-b885-622b8fb88bda',
						],
					},
				}
			)
		).toEqual([
			{
				id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c55134',
				title: 'JavaScript Advanced',
				description: `JavaScript Course Description`,
				creationDate: '10/6/2022',
				duration: 120,
				authors: [
					'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
					'9987de6a-b475-484a-b885-622b8fb88bda',
				],
			},
			{
				id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c55135',
				title: 'React Advanced',
				description: `React Course Description`,
				creationDate: '11/6/2022',
				duration: 120,
				authors: [
					'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
					'9987de6a-b475-484a-b885-622b8fb88bda',
				],
			},
		]);
	});

	it('should handle GET_COURSES and returns new state', () => {
		expect(
			coursesReducer([], {
				type: 'GET_COURSES',
				payload: [
					{
						id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c55135',
						title: 'React Advanced',
						description: `React Course Description`,
						creationDate: '11/6/2022',
						duration: 120,
						authors: [
							'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
							'9987de6a-b475-484a-b885-622b8fb88bda',
						],
					},
				],
			})
		).toEqual([
			{
				id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c55135',
				title: 'React Advanced',
				description: `React Course Description`,
				creationDate: '11/6/2022',
				duration: 120,
				authors: [
					'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
					'9987de6a-b475-484a-b885-622b8fb88bda',
				],
			},
		]);
	});
});
