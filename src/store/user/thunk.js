import { fetchedLogoutUser, fetchedUserProfile, logIn } from '../../services';
import { setUserProfile, userLogIn, userLogOut } from './actionCreators';

import { storageName } from '../../constants';
import {
	changeMessageSeverity,
	changeMessageText,
	showMessage,
} from '../messages/actionCreators';
import { asyncHideMessage } from '../messages/thunk';

export const logoutUserThunk = (token) => {
	return (dispatch) => {
		fetchedLogoutUser(token)
			.then(() => {
				dispatch(userLogOut());
				dispatch(changeMessageText('Bye-bye!'));
				dispatch(changeMessageSeverity('info'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			})
			.catch((e) => {
				dispatch(changeMessageText(e.message));
				dispatch(changeMessageSeverity('error'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			});
	};
};

export const loginUserThunk = (form) => {
	return (dispatch) => {
		logIn(form)
			.then((data) => {
				dispatch(userLogIn({ token: data.result, ...data.user }));
				fetchedUserProfile(data.result)
					.then((userProfile) => {
						dispatch(
							setUserProfile({
								name: userProfile.result.name,
								email: userProfile.result.email,
								role: userProfile.result.role,
							})
						);
						localStorage.setItem(
							storageName,
							JSON.stringify({
								user: data.user,
								token: data.result,
							})
						);
						dispatch(
							changeMessageText(
								`Welcome, ${userProfile.result.name || 'Admin'}!`
							)
						);
						dispatch(changeMessageSeverity('success'));
						dispatch(showMessage());
						dispatch(asyncHideMessage());
					})
					.catch((e) => {
						dispatch(changeMessageText(e.message));
						dispatch(changeMessageSeverity('error'));
						dispatch(showMessage());
						dispatch(asyncHideMessage());
					});
			})
			.catch((e) => {
				dispatch(changeMessageText(e.message));
				dispatch(changeMessageSeverity('error'));
				dispatch(showMessage());
				dispatch(asyncHideMessage());
			});
	};
};
